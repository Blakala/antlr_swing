tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer ifCounter = 0;
  Integer whileCounter = 0;
  Integer doWhileCounter = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name={$e},declarations={$d});

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> declare(id={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> subtract(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> multiply(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divide(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {globals.hasSymbol($i1.text)}? -> setVariable(id={$ID.text},value={$e2.st})
        | INT           -> getInt(i={$INT.text})
        | ID                       {globals.hasSymbol($ID.text)}? -> getVariable(id={$ID.text})
        | ^(EQUALS e1=expr e2=expr)                      -> equals(p1={$e1.st},p2={$e2.st})
        | ^(NOT_EQUALS e1=expr e2=expr)                  -> not_equals(p1={$e1.st},p2={$e2.st})
        | ^(IF e1=expr e2=expr e3=expr?) {ifCounter++;}  -> if(cond={$e1.st}, p1={$e2.st}, p2={$e3.st}, cnt={ifCounter.toString()})
        | ^(WHILE e1=expr e2=expr) {whileCounter++;}     -> whileLoop(p1={$e1.st}, p2={$e2.st}, cnt={whileCounter.toString()})
        | ^(DO e1=expr e2=expr) {doWhileCounter++;}   -> doWhileLoop(p1={$e1.st}, p2={$e2.st}, cnt={whileCounter.toString()})
    ;
    
