grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : expr NL -> expr

    | PRINT expr NL -> ^(PRINT expr)
    | ifBlock NL -> ifBlock
    | whileLoop NL -> whileLoop
    | doWhileLoop NL -> doWhileLoop
//    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)

    | NL ->
    ;

ifBlock
    : IF^ ifCondition THEN! expr (ELSE! expr)? END!
    ;

whileLoop
    : WHILE^ ifCondition THEN! expr
    ;

doWhileLoop
    : DO^ expr WHILE! ifCondition
    ;

ifCondition
    : expr
    (   EQUALS^     expr
    |   NOT_EQUALS^ expr
    )*
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : powExpr
      ( MUL^ powExpr
      | DIV^ powExpr
      )*
    ;

powExpr
  : atom (POW^ atom)*;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

PRINT : 'print';

VAR :'var';

IF
  : 'IF'
  ;

EQUALS
  : '=='
  ;

NOT_EQUALS
  : '!='
  ;

ELSE
  : 'ELSE'
  ;

THEN
  : 'THEN'
  ;

END
  : 'END'
  ;

WHILE
  : 'WHILE'
  ;
  
DO
  : 'DO'
  ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;

POW
  : '^'
  ;