tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (drukowanie | expr | zmienne)* ;


drukowanie:  
          ^(PRINT e1=expr) {drukuj ($e1.text + " = " + $e1.out.toString());};

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = add($e1.out,$e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = substract($e1.out,$e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = multiply($e1.out,$e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = divide($e1.out,$e2.out);}
        | ^(POW   e1=expr e2=expr) {$out = power($e1.out,$e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = getVariable($ID.text);}
        ;catch [Exception e] {drukuj("Wyjątek: "+e.getMessage());$out=Integer.MAX_VALUE;}
        
zmienne :
        ^(VAR i1=ID) {createVariable($i1.text);}
        ^(PODST i1=ID   e2=expr) {setVariable($i1.text, $e2.out);} ;
        

        
