package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;


public class MyTreeParser extends TreeParser {

	protected GlobalSymbols symbols = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

    protected Integer add(Integer e1, Integer e2) {

		return e1 + e2;

	}

	protected Integer substract(Integer e1, Integer e2) {

		return e1 - e2;

	}

	protected Integer multiply(Integer e1, Integer e2) {

		return e1 * e2;

	}

	protected Integer divide(Integer e1, Integer e2) throws Exception {

		if(e2 == 0)
			
			throw new Exception("Cholero, nie dziel przez zero!");

		return e1 / e2;

	}

	protected Integer power(Integer e1, Integer e2) {

		return (int) Math.pow(e1, e2);

	}

	protected void createVariable(String nazwa) {

		this.symbols.newSymbol(nazwa);

	}

	protected Integer setVariable(String nazwa, Integer e1) {

		this.symbols.setSymbol(nazwa, e1);

		return e1;

	}

	protected Integer getVariable(String nazwa) {

		return this.symbols.getSymbol(nazwa);

	}
     
	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
}
